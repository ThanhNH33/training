using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] public GameObject prefab;
    [SerializeField] public GameObject plane;
    private Bounds spawnArea;
    // Start is called before the first frame update
    void Start()
    {
        spawnArea = plane.GetComponent<MeshCollider>().bounds;
        prefab.SetActive(true);
        for (int i = 0; i < 4; i++)
        {
            Instantiate(prefab, randomSpawnPosition(), Quaternion.identity);
        }
    }

    Vector3 randomSpawnPosition()
    {
        float x = Random.Range(spawnArea.min.x, spawnArea.max.x);
        float z = Random.Range(spawnArea.min.z, spawnArea.max.z);
        float y = 0.5f;

        return new Vector3(x, y, z);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
