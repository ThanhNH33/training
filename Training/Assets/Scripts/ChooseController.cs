using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ChooseController : MonoBehaviour
{
    [SerializeField] public InputField mainInputField;
    public static string text;
    public static int number;
    // Start is called before the first frame update
    void Start()
    {
        mainInputField.onEndEdit.AddListener(delegate { LockInput(mainInputField); });
    }

    public void OnClickBtn(int num)
    {
        number = num;
        SceneManager.LoadScene("PlayScene");
    }

    void LockInput(InputField input)
    {
        text = input.text;

    }
}
