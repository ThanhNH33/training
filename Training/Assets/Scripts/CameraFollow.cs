using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public Transform ball;
    private Vector3 cameraVector3;
    // Start is called before the first frame update
    void Start()
    {
        cameraVector3 = ball.transform.position - transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = ball.transform.position - cameraVector3;
    }
}
