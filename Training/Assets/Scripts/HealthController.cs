using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthController : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject ball;
    private BallController ballController;
    void Start()
    {
        ballController = ball.GetComponent<BallController>();
    }

    public void OnCollisionEnter(Collision collision)
    {
        ballController.healthHealth();
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
