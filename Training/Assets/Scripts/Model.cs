using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Model", menuName = "Model")]
public class Model : ScriptableObject
{
    public string Name;
    public Sprite Sprite;
    public int Speed;
}
