using System;
using System.Collections;
using System.Threading.Tasks;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class BallController : MonoBehaviour
{
    [SerializeField] public Material[] materials;
    [SerializeField] public Text Name;
    [SerializeField] public float speed;
    [SerializeField] public float maxSpeed;
    [SerializeField] public float jumpHeight;
    [SerializeField] public float maxHealth;
    [SerializeField] public HeathBar heathBar;
    [SerializeField] public int perInMinus;


    private float curHealth;
    private Rigidbody rb;
    private KeyCode[] inputKeys;
    private Vector3[] directionsForKeys;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Renderer>().material = materials[ChooseController.number];
        Name.text = ChooseController.text;
        inputKeys = new KeyCode[] { KeyCode.W, KeyCode.A, KeyCode.S, KeyCode.D };
        directionsForKeys = new Vector3[] { Vector3.forward, Vector3.left, Vector3.back, Vector3.right };
        rb = GetComponent<Rigidbody>();
        curHealth = maxHealth;
        MinusBlood();
    }
    // Update is called once per frame
    private void FixedUpdate()
    {
        for (int i = 0; i < inputKeys.Length; i++)
        {
            var key = inputKeys[i];

            // 2
            if (Input.GetKey(key))
            {
                // 3
                Vector3 movement = directionsForKeys[i] * speed * Time.deltaTime;
                movePlayer(movement);
            }
            if (Input.GetKey("space"))
            {
                rb.AddForce(Vector3.up * jumpHeight * Time.deltaTime);
            }
        }
    }

    private void MinusBlood()
    {
        curHealth -= perInMinus;
        heathBar.UpdateHealthBar((float)curHealth / (float)maxHealth);
        WaitForSecond();
    }

    public void healthHealth()
    {
        curHealth += 10;
        heathBar.UpdateHealthBar((float)curHealth / (float)maxHealth);
    }

    private async void WaitForSecond()
    {
        await Task.Delay(1000);
        if(curHealth > 0)
        {
            ReCallMethod(MinusBlood);
        }
    }

    private void ReCallMethod(Action action)
    {
        action();
    }
    private void movePlayer(Vector3 movement)
    {
        if (rb.velocity.magnitude * speed > maxSpeed)
        {
            rb.AddForce(movement * -1);
        }
        else
        {
            rb.AddForce(movement);
        }
    }
}
