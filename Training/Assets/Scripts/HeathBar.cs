using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeathBar : MonoBehaviour
{
    public Image HealthBar;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void UpdateHealthBar(float fraction)
    {
        HealthBar.fillAmount = fraction;
    }
}
